package org.ujazdowski.generator.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface GenerateClass {

  String packageSuffix() default "snapshot";

  String classSuffix() default "Snapshot";

  boolean finalFields() default true;

  Class<? extends Annotation>[] annotations() default {};

  Class<?> supperClass();

  Class<?>[] supperClassParameters() default {};

  boolean getters() default false;

  boolean setters() default false;

  boolean allArgsConstructor() default false;

}
