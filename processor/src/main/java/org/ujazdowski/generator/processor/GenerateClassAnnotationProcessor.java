package org.ujazdowski.generator.processor;

import static javax.lang.model.util.ElementFilter.fieldsIn;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;
import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Generated;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import org.ujazdowski.generator.annotation.GenerateClass;


public class GenerateClassAnnotationProcessor extends GeneratorProcessor {

  private Set<? extends Element> annotatedElements;

  public GenerateClassAnnotationProcessor(Types typeUtils, Elements elementUtils, Filer filer, Messager messager) {
    super(typeUtils, elementUtils, filer, messager);
  }

  private ClassDetails getClassDetails(TypeElement classElem) {
    String parentQualifiedClassName = classElem.getQualifiedName().toString();
    String parentClassName = classElem.getSimpleName().toString();
    String parentPackage = parentQualifiedClassName.substring(0, parentQualifiedClassName.indexOf(parentClassName));
    return new ClassDetails(this.getClass(), parentPackage, parentClassName,
        SnapshotDataFactory.create(classElem.getAnnotation(GenerateClass.class)));
  }

  @Override
  public boolean process(RoundEnvironment roundEnv) {
    try {
      annotatedElements = Collections.unmodifiableSet(roundEnv.getElementsAnnotatedWith(GenerateClass.class));
      for (Element annotatedElement : annotatedElements) {
        generateClass(annotatedElement);
      }
    } catch (Exception ex) {
      messager.printMessage(Diagnostic.Kind.ERROR, ex.getMessage());
    }
    return true;
  }

  private void generateClass(Element element) throws IOException {
    ClassDetails classDetails = getClassDetails((TypeElement) element);
    TypeSpec.Builder classBuilder = classBuilder(classDetails);
    addSupperClass(classDetails, classBuilder);
    addAnnotations(classDetails, classBuilder);
    List<VariableElement> fields = fieldsIn(element.getEnclosedElements());
    addFields(classDetails, classBuilder, fields);
    addMethods(classDetails.classData(), classBuilder);
    JavaFile.builder(classDetails.packageName(), classBuilder.build())
        .skipJavaLangImports(true)
        .indent("\t")
        .build()
        .writeTo(filer);
  }

  private TypeSpec.Builder classBuilder(ClassDetails classDetails) {
    return TypeSpec.classBuilder(classDetails.className())
        .addModifiers(Modifier.PUBLIC);
  }

  private void addSupperClass(ClassDetails classDetails, TypeSpec.Builder classBuilder) {
    Optional.ofNullable(classDetails.classData().supperClass())
        .ifPresent(superclass -> {
              classBuilder.superclass(
                  ParameterizedTypeName.get(
                      ClassName.bestGuess(superclass),
                      classDetails.classData().supperClassParameters().stream().map(ClassName::bestGuess).toList().toArray(new TypeName[]{})
                  )
              );
            }
        );

  }

  private void addAnnotations(ClassDetails classDetails, TypeSpec.Builder classBuilder) {
    classDetails.classData().annotations()
        .forEach(annotation -> classBuilder.addAnnotation(ClassName.bestGuess(annotation)));
    classBuilder.addAnnotation(generatedAnnotation(classDetails));
  }

  private AnnotationSpec generatedAnnotation(ClassDetails classDetails) {
    return AnnotationSpec.builder(Generated.class)
        .addMember("value", "$S", classDetails.annotationProcessor())
        .addMember("date", "$S", Instant.now().toString())
        .build();
  }

  private void addFields(ClassDetails classDetails, TypeSpec.Builder classBuilder, List<VariableElement> fields) {
    fields.forEach(field -> {
      String fieldName = field.getSimpleName().toString();
      TypeName fieldType = fieldType(field);
      Set<Modifier> modifiers = new HashSet<>();
      modifiers.add(Modifier.PRIVATE);
      if (classDetails.classData().isFinalFields()) {
        modifiers.add(Modifier.FINAL);
      }

      classBuilder.addField(
          FieldSpec.builder(fieldType, fieldName, modifiers.toArray(new Modifier[]{}))
              .build()
      );
    });
  }

  private TypeName fieldType(VariableElement element) {
    if (element.asType() instanceof DeclaredType declaredType) {
      List<TypeName> typeNames = declaredType.getTypeArguments().stream()
          .map(this::transformType)
          .toList();

      if (typeNames.isEmpty()) {
        return transformType(declaredType);
      }
      return ParameterizedTypeName.get(ClassName.bestGuess(declaredType.asElement().toString()), typeNames.toArray(new TypeName[]{}));
    }

    return TypeName.get(element.asType());
  }

  private TypeName transformType(TypeMirror typeMirror) {
    if (typeMirror instanceof DeclaredType declaredType) {
      Element element = declaredType.asElement();
      if (annotatedElements.contains(element)) {
        GenerateClass generateClass = element.getAnnotation(GenerateClass.class);
        return ClassName.bestGuess(element.toString().replace(
                element.getSimpleName(),
                generateClass.packageSuffix() + "." + element.getSimpleName() + generateClass.classSuffix()
            )
        );
      }
    }
    return TypeName.get(typeMirror);
  }

  private void addMethods(ClassData classData, Builder classBuilder) {
    if (classData.getters()) {
      addGetterMethods(classBuilder);
    }
    if (classData.setters() && !classData.isFinalFields()) {
      addSetterMethods(classBuilder);
    }
    if (classData.allArgsConstructor()) {
      addAllArgsConstructor(classBuilder);
    }
  }

  private void addGetterMethods(Builder classBuilder) {
    classBuilder.fieldSpecs.stream().map(fieldSpec -> MethodSpec.methodBuilder("get" + capitaliseFirstLetter(fieldSpec.name))
        .addModifiers(Modifier.PUBLIC)
        .returns(fieldSpec.type)
        .addStatement("return this.$N", fieldSpec.name)
        .build()
    ).forEach(classBuilder::addMethod);
  }

  private void addSetterMethods(Builder classBuilder) {
    classBuilder.fieldSpecs.stream().map(fieldSpec -> MethodSpec.methodBuilder("set" + capitaliseFirstLetter(fieldSpec.name))
        .addModifiers(Modifier.PUBLIC)
        .returns(void.class)
        .addParameter(fieldSpec.type, fieldSpec.name)
        .addStatement("this.$N = $N", fieldSpec.name, fieldSpec.name)
        .build()
    ).forEach(classBuilder::addMethod);
  }

  private void addAllArgsConstructor(Builder classBuilder) {
    MethodSpec.Builder constructor = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);

    classBuilder.fieldSpecs.forEach(fieldSpec -> {
      constructor.addParameter(fieldSpec.type, fieldSpec.name)
          .addStatement("this.$N = $N", fieldSpec.name, fieldSpec.name);
    });

    classBuilder.addMethod(constructor.build());
  }

  private static String capitaliseFirstLetter(String value) {
    return value.substring(0, 1).toUpperCase() + value.substring(1);
  }

}
