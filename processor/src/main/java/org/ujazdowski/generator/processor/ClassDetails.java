package org.ujazdowski.generator.processor;

public class ClassDetails {

  private final Class<?> annotationProcessor;
  private final String parentPackageName;
  private final String parentClassName;
  private final ClassData classData;

  public ClassDetails(Class<?> annotationProcessor, String parentPackageName, String parentClassName, ClassData snapshot) {
    this.annotationProcessor = annotationProcessor;
    this.parentPackageName = parentPackageName;
    this.parentClassName = parentClassName;
    this.classData = snapshot;
  }

  public String packageName() {
    return parentPackageName + classData.packageSuffix();
  }

  public String className() {
    return parentClassName + classData.classSuffix();
  }

  public String fileName() {
    return  packageName()  + "." + className();
  }

  public String annotationProcessor() {
    return annotationProcessor.getCanonicalName();
  }

  public ClassData classData() {
    return classData;
  }

}
