package org.ujazdowski.generator.processor;

import java.util.Collection;

public class ClassData {

  private final String packageSuffix;
  private final String classSuffix;
  private final boolean finalFields;
  private final String supperClass;
  private final Collection<String> supperClassParameters;
  private final Collection<String> annotations;
  private final boolean getters;
  private final boolean setters;
  private final boolean allArgsConstructor;

  public ClassData(String packageSuffix, String classSuffix, boolean finalFields, String supperClass, Collection<String> supperClassParameters,
      Collection<String> annotations, boolean getters, boolean setters, boolean allArgsConstructor) {
    this.packageSuffix = packageSuffix;
    this.classSuffix = classSuffix;
    this.finalFields = finalFields;
    this.supperClass = supperClass;
    this.supperClassParameters = supperClassParameters;
    this.annotations = annotations;
    this.getters = getters;
    this.setters = setters;
    this.allArgsConstructor = allArgsConstructor;
  }

  public String packageSuffix() {
    return packageSuffix;
  }

  public String classSuffix() {
    return classSuffix;
  }

  public boolean isFinalFields() {
    return finalFields;
  }

  public String supperClass() {
    return supperClass;
  }

  public Collection<String> supperClassParameters() {
    return supperClassParameters;
  }

  public Collection<String> annotations() {
    return annotations;
  }

  public boolean getters() {
    return getters;
  }

  public boolean setters() {
    return setters;
  }

  public boolean allArgsConstructor() {
    return allArgsConstructor;
  }

}
