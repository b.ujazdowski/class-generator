package org.ujazdowski.generator.processor;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.MirroredTypesException;
import org.ujazdowski.generator.annotation.GenerateClass;

public class SnapshotDataFactory {

  public static ClassData create(GenerateClass generateSnapshot) {
    return new ClassData(
        generateSnapshot.packageSuffix(),
        generateSnapshot.classSuffix(),
        generateSnapshot.finalFields(),
        retrieveClassName(generateSnapshot::supperClass),
        retrieveClassNames(generateSnapshot::supperClassParameters),
        retrieveClassNames(generateSnapshot::annotations),
        generateSnapshot.getters(),
        generateSnapshot.setters(),
        generateSnapshot.allArgsConstructor()
    );
  }

  private static Collection<String> retrieveClassNames(Getter<Class<?>[]> getter) {
    try {
      return Arrays.stream(getter.apply()).map(annotation -> retrieveClassName(() -> annotation)).toList();
    } catch (MirroredTypesException mte) {
      return ((List<DeclaredType>) mte.getTypeMirrors()).stream().map(SnapshotDataFactory::retrieveClassName).toList();
    }
  }

  private static String retrieveClassName(Getter<Class<?>> getter) {
    String result;
    try {
      result = getter.apply().getCanonicalName();
    } catch (MirroredTypeException mte) {
      DeclaredType classTypeMirror = (DeclaredType) mte.getTypeMirror();
      result = retrieveClassName(classTypeMirror);
    }
    return result;
  }

  private static String retrieveClassName(DeclaredType type) {
    TypeElement classTypeElement = (TypeElement) type.asElement();
    return classTypeElement.getQualifiedName().toString();
  }

  @FunctionalInterface
  private interface Getter<T> {

    T apply();

  }

}
