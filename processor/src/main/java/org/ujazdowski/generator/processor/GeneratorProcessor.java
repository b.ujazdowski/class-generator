package org.ujazdowski.generator.processor;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

public abstract class GeneratorProcessor {

    protected final Types typeUtils;
    protected final Elements elementUtils;
    protected final Filer filer;
    protected final Messager messager;

    protected GeneratorProcessor(Types typeUtils, Elements elementUtils, Filer filer, Messager messager) {
        this.typeUtils = typeUtils;
        this.elementUtils = elementUtils;
        this.filer = filer;
        this.messager = messager;
    }

    abstract boolean process(RoundEnvironment roundEnv);

}
